# സ്വതന്ത്രം

#### How to develop locally

1. Make sure you have the following prerequisites satisfied
  1. Ruby
  1. Bundler (`gem install bundler`)
1. Fork this repository and clone it.
1. Change to a new branch
1. Install required gems

    ```
    $ bundle install
    ```
1. Make necessary changes
1. Run the server

    ```
    $ jekyll serve --watch
    ```
1. Visit http://localhost:4000 to view the site
1. If everything is ready, push the branch and create a Merge Request
